from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.http import Http404
from django.views.generic import (CreateView,
                                  DetailView,
                                  ListView,
                                  UpdateView,
                                  DeleteView,
                                  RedirectView,
                                  View)
from .models import Messages
from .forms import MessagesForm
from django.contrib.auth.mixins import LoginRequiredMixin
from videos.mixins import MemberRequiredMixin, StaffRequiredMixin


# Create your views here.
# Create your views here.
class MessagesCreateView(StaffRequiredMixin, CreateView):
    model = Messages
    form_class = MessagesForm

    # def get_context_data(self, **kwargs):
    #     data = super(CourseCreateView, self).get_context_data(**kwargs)
    #     if self.request.POST:
    #         data['lecture'] = CourseLectureFormSet(self.request.POST)
    #     else:
    #         data['lecture'] = CourseLectureFormSet()
    #     return data

    def form_valid(self, form):
        obj = form.save(commit=False)
        # obj.user = self.request.user
        obj.save()
        return super(MessagesCreateView, self).form_valid(form)


class MessagesListView(LoginRequiredMixin, ListView):
    def get_queryset(self):
        user = self.request.user
        qs = Messages.objects.filter(user=user).order_by('-id')
        if user.is_staff or user.is_superuser:
            qs = Messages.objects.all().order_by('-id')
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(MessagesListView, self).get_context_data(*args, **kwargs)
        return context


class MessagesDetailView(StaffRequiredMixin, DetailView):
    model = Messages
    queryset = Messages.objects.all()


class MessagesUpdateView(StaffRequiredMixin, UpdateView):
    queryset = Messages.objects.all()
    form_class = MessagesForm


class MessagesDeleteView(StaffRequiredMixin, DeleteView):
    queryset = Messages.objects.all()
    success_url = reverse_lazy('eswahiligigs_messages:list')
