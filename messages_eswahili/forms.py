from django import forms
from .models import Messages


class MessagesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user'].widget.attrs['class'] = 'form-control'
        self.fields['message'].widget.attrs['class'] = 'form-control'
        self.fields['title'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Messages
        fields = ['title', 'message', 'user']
        exclude = ()
