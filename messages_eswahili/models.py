from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse


# Create your models here.

class Messages(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=100)
    message = models.TextField(blank=False)
    read = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message

    def get_absolute_url(self):
        return reverse('eswahiligigs_messages:detail', kwargs={'pk': self.pk})
