"""
Adds simple form view, which communicates with Braintree.

There are four steps to finally process a transaction:

1. Create a client token (views.py)
2. Send it to Braintree (js)
3. Receive a payment nonce from Braintree (js)
4. Send transaction details and payment nonce to Braintree (views.py)

"""
import braintree
from django.shortcuts import render
from django.conf import settings
from .emails import SubscriptionEmail
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.utils.html import format_html

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib import messages
from .models import UserCheckout, Subscription
from django.contrib.auth.mixins import LoginRequiredMixin
from videos.mixins import MemberRequiredMixin
from .mixins import UserSubscribedMixin, UserNotSubscribedMixin

from . import forms

# Ha! There it is. This allows you to switch the
# Braintree environments by changing one setting
if settings.BRAINTREE_PRODUCTION:
    braintree_env = braintree.Environment.Production
else:
    braintree_env = braintree.Environment.Sandbox

# Configure Braintree
braintree.Configuration.configure(
    braintree_env,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY,
)


class CheckoutView(LoginRequiredMixin, UserSubscribedMixin, generic.FormView):
    """This view lets the user initiate a payment."""
    form_class = forms.CheckoutForm
    template_name = 'billing/checkout.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        # We need the user to assign the transaction
        self.user = request.user

        # Generate a client token. We'll send this to the form to
        # finally generate the payment nonce
        # You're able to add something like ``{"customer_id": 'foo'}``,
        # if you've already saved the ID
        qs_user_checkout = UserCheckout.objects.filter(email=self.user.email)
        if not qs_user_checkout.exists():
            self.braintree_client_token = braintree.ClientToken.generate({})
        else:
            braintree_id = qs_user_checkout.first().braintree_id
            self.braintree_client_token = braintree.ClientToken.generate({"customer_id": braintree_id})

        return super(CheckoutView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(CheckoutView, self).get_context_data(**kwargs)
        ctx.update({
            'braintree_client_token': self.braintree_client_token,
        })
        return ctx

    def form_valid(self, form):
        # Braintree customer info
        # You can, for sure, use several approaches to gather customer infos
        # For now, we'll simply use the given data of the user instance
        qs_user_checkout = UserCheckout.objects.filter(email=self.user.email)
        if not qs_user_checkout.exists():
            customer_kwargs = {
                "email": self.user.email,
                "payment_method_nonce": form.cleaned_data['payment_method_nonce'],
            }

            # Create a new Braintree customer
            # In this example we always create new Braintree users
            # You can store and re-use Braintree's customer IDs, if you want to
            result = braintree.Customer.create(customer_kwargs)
            if not result.is_success:
                # Ouch, something went wrong here
                # I recommend to send an error report to all admins
                # , including ``result.message`` and ``self.user.email``
                staff_email_body = format_html(
                    "The following user with email {}. could not be created . due to the \
                     following reasons {}.".format(str(self.user.email), result.message))
                email_body = format_html("This is to notify you that your transactions <b> Could Not be Completed </b>.\
                                    due to the following reasons <br>  <b> {} </b>. <br> <br>You can always enroll to our systems as we are always adding more content.\
                                      We hope to have you back as you sharpen your swahili skills.".format(
                    result.message))
                SubscriptionEmail.send_staff_email(staff_email_body, email_subject="Failed Braintree Customer Create")
                SubscriptionEmail.send_user_email_on_subscription_cancel(email_body=email_body,
                                                                         email_subject=" Failed Transaction",
                                                                         send_to=[
                                                                             str(self.request.user.email)])
                print('something went wrong')

                context = self.get_context_data()
                # We re-generate the form and display the relevant braintree error
                context.update({
                    'form': self.get_form(self.get_form_class()),
                    'braintree_error': u'{} {}'.format(
                        result.message, 'Please get in contact.')
                })
                messages.add_message(self.request, messages.ERROR, result.message)
                return self.render_to_response(context)

            # If the customer creation was successful you might want to also
            # add the customer id to your user profile
            customer_id = result.customer.id
            customer_payment_methods_token = result.customer.payment_methods[0].token
            UserCheckout.objects.create(user=self.user, email=self.user.email, braintree_id=customer_id,
                                        braintree_customer_token=customer_payment_methods_token)

        """
        Create a new transaction and submit it.
        I don't gather the whole address in this example, but I can
        highly recommend to do that. It will help you to avoid any
        fraud issues, since some providers require matching addresses

        """
        customer_payment_token = qs_user_checkout.first().braintree_customer_token
        address_dict = {
            "first_name": self.user.first_name,
            "last_name": self.user.last_name,
            "street_address": 'street',
            "extended_address": 'street_2',
            "locality": 'city',
            "region": 'state_or_region',
            "postal_code": 'postal_code',
            "country_code_alpha2": 'alpha2_country_code',
            "country_code_alpha3": 'alpha3_country_code',
            "country_name": 'country',
            "country_code_numeric": 'numeric_country_code',
        }

        # You can use the form to calculate a total or add a static total amount
        # I'll use a static amount in this example
        # result = braintree.Transaction.sale({
        #     "customer_id": customer_id,
        #     "amount": 100,
        #     "payment_method_nonce": form.cleaned_data['payment_method_nonce'],
        #     # "descriptor": {
        #     #     # Definitely check out https://developers.braintreepayments.com/reference/general/validation-errors/all/python#descriptor
        #     #     "name": "eswahiligigs.*courses",
        #     # },
        #     # "billing": address_dict,
        #     # "shipping": address_dict,
        #     "options": {
        #         # Use this option to store the customer data, if successful
        #         'store_in_vault_on_success': True,
        #         # Use this option to directly settle the transaction
        #         # If you want to settle the transaction later, use ``False`` and later on
        #         # ``braintree.Transaction.submit_for_settlement("the_transaction_id")``
        #         'submit_for_settlement': True,
        #     },
        # })
        if form.cleaned_data['plan'] == 'monthly_plan':
            result = braintree.Subscription.create({
                "payment_method_token": customer_payment_token,
                "plan_id": "monthly_plan"
            })
        else:
            print(form.cleaned_data['plan'])
            result = braintree.Subscription.create({
                "payment_method_token": customer_payment_token,
                "plan_id": "quartely_plan"
            })

        if not result.is_success:
            # Card could've been declined or whatever
            # I recommend to send an error report to all admins
            # , including ``result.message`` and ``self.user.email``
            staff_email_body = format_html("The subscription for the user {} <b> Could Not be Completed </b>.\
                                due to the following reasons <br>  <b> {} </b>. <br> .".format(
                str(self.request.user.email), result.message))
            email_body = format_html("This is to notify you that your subscription plan <b> Could Not be Completed </b>.\
                                due to the following reasons <br>  <b> {} </b>. <br> <br>You can always enroll to our systems as we are always adding more content.\
                                  We hope to have you back as you sharpen your swahili skills.".format(result.message))
            SubscriptionEmail.send_staff_email(email_body=staff_email_body,
                                               email_subject="Braintree Failed Subscription  Create")
            SubscriptionEmail.send_user_email_on_subscription_cancel(email_body=email_body,
                                                                     email_subject=" Failed  Subscription",
                                                                     send_to=[
                                                                         str(self.request.user.email)])
            context = self.get_context_data()
            context.update({
                'form': self.get_form(self.get_form_class()),
                'braintree_error': (
                    'Your payment could not be processed. Please check your'
                    ' input or use another payment method and try again.')
            })
            messages.add_message(self.request, messages.ERROR, result.message)
            return self.render_to_response(context)

        # Finally there's the transaction ID
        # You definitely want to send it to your database
        # transaction_id = result.Transaction.id
        transaction = result.subscription.transactions[0]
        Subscription.objects.create(user=self.request.user, email=self.request.user.email, status='paid',
                                    completed=True,
                                    transaction_id=transaction.id, payment_type=transaction.payment_instrument_type,
                                    plan=transaction.plan_id, amount=transaction.amount,
                                    billing_period_end_date=result.subscription.billing_period_end_date,
                                    subscription_id=result.subscription.id
                                    )
        # Now you can send out confirmation emails or update your metrics
        ### send emails after successful done
        SubscriptionEmail.send_user_email_on_subscription(plan=transaction.plan_id,
                                                          send_to=[str(self.request.user.email)],
                                                          send_bcc=None)  # send_bcc=['team@eswahiligigs.com']
        # or do whatever makes you and your customers happy :)
        return super(CheckoutView, self).form_valid(form)

    def get_success_url(self):
        # Add your preferred success url
        messages.add_message(self.request, messages.SUCCESS, 'Thanks for enrolling to Eswahiligigs.Happy learning')
        return reverse('dashboard')


class CancelSubscriptionView(LoginRequiredMixin,UserNotSubscribedMixin, generic.View):

    def get(self, request, slug, *args, **kwargs):
        result = braintree.Subscription.cancel(slug)
        if not result.is_success:
            # to change to redirect to correct dashboard message and send email
            email_body = format_html("This is to notify you that your subscription plan <b> Could Not be cancelled </b>.\
                    due to the following reasons <br>  <b> {} </b>. <br> <br>You can always enroll to our systems as we are always adding more content.\
                      We hope to have you back as you sharpen your swahili skills.".format(result.message))
            SubscriptionEmail.send_user_email_on_subscription_cancel(email_body=email_body,
                                                                     email_subject=" Failed Cancelled Subscription",
                                                                     send_to=[
                                                                         str(self.request.user.email)])
            messages.add_message(self.request, messages.ERROR, result.message)
            return HttpResponseRedirect(reverse('payments:checkout'))
        # to change to redirect to correct dashboard message and send email
        email_body = format_html("This is to notify you that your subscription plan has been <b>cancelled</b>.\
         <br> You can always enroll to our systems as we are always adding more content.\
          We hope to have you back as you sharpen your swahili skills.")
        SubscriptionEmail.send_user_email_on_subscription_cancel(email_body=email_body,
                                                                 email_subject="Cancelled Subscription", send_to=[
                str(self.request.user.email)])  # send_bcc=['team@eswahiligigs.com']

        messages.add_message(self.request, messages.SUCCESS, 'Subscription cancelled')
        return HttpResponseRedirect(reverse('payments:checkout'))


class CheckActiveSubscription(LoginRequiredMixin, generic.View):

    def get(self, request, *args, **kwargs):
        qs = UserCheckout.objects.all().filter(email=request.user.email)
        status = False
        if qs.exists():
            status = self.is_subscribed(qs.first().braintree_id)
            print(status)
        if status:
            return HttpResponseRedirect(redirect('home', kwargs={'active': status}))

    @staticmethod
    def is_subscribed(customer_id):
        customer = braintree.Customer.find(customer_id)
        for payment in customer.payment_methods:
            if payment.subscriptions:
                subscription_status = payment.subscriptions[0].status
                if subscription_status == 'Active' or subscription_status == 'Pending':
                    return True
        return False
