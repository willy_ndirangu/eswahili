# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-21 07:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0007_subscription_subscription_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='email',
            field=models.EmailField(blank=True, max_length=120, null=True),
        ),
    ]
