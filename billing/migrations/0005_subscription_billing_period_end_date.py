# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-20 17:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0004_auto_20190220_0655'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='billing_period_end_date',
            field=models.TimeField(blank=True, null=True),
        ),
    ]
