from django.contrib import admin
from .models import UserCheckout, Subscription

# Register your models here.
admin.site.register(Subscription)
admin.site.register(UserCheckout)
