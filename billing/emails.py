from django.core.mail import EmailMessage, send_mass_mail, EmailMultiAlternatives, send_mail
from django.template.loader import render_to_string
from django.contrib.auth.models import User


class SubscriptionEmail:
    @staticmethod
    def send_user_email_on_subscription(plan, send_to, send_bcc=None):
        html_content = render_to_string('billing/mails/subscription.html',
                                        {'plan': plan})
        subject, from_email, to = ('Eswahiligigs Subscription', 'info@swahiligigs', send_to)
        msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=send_bcc)
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_user_email_on_subscription_cancel(email_body, email_subject, send_to, send_bcc=None):
        html_content = render_to_string('billing/mails/cancel_subscription.html', {'body': email_body})
        subject, from_email, to = (email_subject, 'info@swahiligigs', send_to)
        msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=send_bcc)
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_staff_email(email_body, email_subject, send_bcc=None):
        staff_members = User.objects.filter(is_staff=True)
        if staff_members.exists():
            staff_emails = [staff.email for staff in staff_members]
            send_to = staff_emails
            html_content = render_to_string('billing/mails/staff_billing_mail.html', {'body': email_body})
            subject, from_email, to = (email_subject, 'info@swahiligigs', send_to)
            msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=send_bcc)
            msg.content_subtype = "html"
            msg.send()
        else:
            send_mail(
                email_subject,
                email_body,
                'info@eswahiligigs.com',
                ['wnwillyndirangu@gmail.com'],
                fail_silently=True,
            )

    # def notify_user_of_request(self, email, name, location, phone_number):
    #     html_content = render_to_string('mails/request_notify_user.html',
    #                                     {'name': name, 'email': email, 'location': location,
    #                                      'phone_number': phone_number})
    #     subject, from_email, to = ('Internet Service request', 'clientservice@lastmile-networks.net', [email])
    #     msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=["support@lastmile-networks.net"])
    #     msg.content_subtype = "html"
    #     msg.send()
