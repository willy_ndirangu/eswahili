from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, post_save
import braintree
from django.core.urlresolvers import reverse
from django.contrib import auth
from .emails import SubscriptionEmail
from django.utils.html import format_html
from .invoices import gen_invoice

# Ha! There it is. This allows you to switch the
# Braintree environments by changing one setting
if settings.BRAINTREE_PRODUCTION:
    braintree_env = braintree.Environment.Production
else:
    braintree_env = braintree.Environment.Sandbox

# Configure Braintree
braintree.Configuration.configure(
    braintree_env,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY,
)


# Create your models here.
class UserCheckout(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    email = models.EmailField(unique=True, max_length=120, null=True, blank=True)
    braintree_id = models.CharField(max_length=120, null=True, blank=True)
    braintree_customer_token = models.CharField(max_length=120, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email


def update_braintree_id(sender, instance, *args, **kwargs):
    if not instance.braintree_id:
        result = braintree.customer.create({
            "email": instance.email,
        })
        if result.is_success:
            instance.braintree_id = result.customer.id
            instance.save()


post_save.connect(update_braintree_id, sender=UserCheckout)
ORDER_CHOICES = (
    ('paid', 'paid'),
    ('created', 'created')
)


class Subscription(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    email = models.EmailField(unique=False, max_length=120, null=True, blank=True)
    status = models.CharField(max_length=120, choices=ORDER_CHOICES)
    completed = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=20)
    subscription_id = models.CharField(max_length=20)
    payment_type = models.CharField(max_length=20, null=True, blank=True)
    plan = models.CharField(max_length=20, null=True, blank=True)
    amount = models.DecimalField(max_digits=10, null=True, blank=True, decimal_places=2)
    billing_period_end_date = models.DateTimeField(null=True, blank=True)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subscription_id

    def get_absolute_url(self):
        return reverse('payments:cancel-subscription', kwargs={'slug': self.subscription_id})


def send_email_on_subscription(sender, instance, *args, **kwargs):
    gen_invoice(invoice_no=instance.subscription_id, subscriber=instance.email, amount=instance.amount,
                details=str(instance.plan) + " membership enrollment")
    staff_email_body = format_html(
        "A user with email {email} has made a subscription to the {plan}  whose amount is  {amount} \
        and payment type is  {payment_type}  and subscription id is <b> {subscription_id} </b> and ends in \
         {billing_period_end_date}".format(email=instance.email, plan=instance.plan, payment_type=instance.payment_type,
                                           amount=instance.amount, subscription_id=instance.subscription_id,
                                           billing_period_end_date=instance.billing_period_end_date))
    SubscriptionEmail.send_staff_email(email_body=staff_email_body, email_subject="New Subscription")


post_save.connect(send_email_on_subscription, sender=Subscription)


def get_subscription_status(self):
    qs = UserCheckout.objects.all().filter(email=self.email)
    status = False
    if qs.exists():
        status = is_subscribed(qs.first().braintree_id)
    return status


def is_subscribed(customer_id):
    customer = braintree.Customer.find(customer_id)
    for payment in customer.payment_methods:
        if payment.subscriptions:
            for payment_subscription in payment.subscriptions:
                subscription_status = payment_subscription.status

                if subscription_status == 'Active' or subscription_status == 'Pending':
                    return True
    return False


auth.models.User.add_to_class('is_subscribed', get_subscription_status)
