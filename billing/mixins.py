from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.shortcuts import redirect

class UserSubscribedMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_subscribed():
            return super(UserSubscribedMixin, self).dispatch(request, *args, **kwargs)

        messages.add_message(request, messages.WARNING, "You already have an active subscription")
        return redirect('dashboard')

class UserNotSubscribedMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_subscribed():
            messages.add_message(request, messages.WARNING, "You dont have an active subscription")
            return redirect('dashboard')
        return super(UserNotSubscribedMixin, self).dispatch(request, *args, **kwargs)