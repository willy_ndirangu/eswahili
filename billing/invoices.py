from django.template.loader import render_to_string
from weasyprint import HTML, CSS
from django.conf import settings
import num2words as nm


def gen_invoice(invoice_no, subscriber, amount, details):
    html = render_to_string('billing/pdfs/invoices.html',
                            {'invoice_no': invoice_no, 'subscriber': subscriber, 'amount': amount, 'details': details,
                             'amount_in_words': nm.num2words(amount)})
    path = settings.MEDIA_ROOT + '/invoices/' + "Invoice_" + str(invoice_no) + "_eswahiligigs" + '.pdf'
    pdf = HTML(string=html).write_pdf(target=path, stylesheets=[CSS(
        settings.STATIC_ROOT + '/css/pdfs.css')])
