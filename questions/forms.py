from django import forms
from .models import Question, Reply


class QuestionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['question'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Question
        fields = ['question']
        exclude = ()


class ReplyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['reply'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Reply
        fields = ['reply']
        exclude = ()
