from django.db import models
from django.conf import settings
from courses.utils import create_question_slug
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse


# Create your models here.
class QuestionQuerySet(models.query.QuerySet):

    def replies(self):
        return self.prefetch_related('reply')


class QuestionManager(models.Manager):
    def get_queryset(self):
        return QuestionQuerySet(self.model, using=self._db)


class Question(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    question = models.CharField(max_length=250, blank=False)
    slug = models.SlugField(blank=True)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = QuestionManager()

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('questions:detail', kwargs={'slug': self.slug})


class Reply(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    question = models.ForeignKey(Question)
    reply = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.reply

    def get_absolute_url(self):
        return reverse('questions:detail', kwargs={'slug': self.question.slug})


def pre_save_question_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_question_slug(instance)


pre_save.connect(pre_save_question_receiver, sender=Question)
