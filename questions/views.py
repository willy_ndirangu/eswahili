from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.http import Http404
from django.views.generic import (CreateView,
                                  DetailView,
                                  ListView,
                                  UpdateView,
                                  DeleteView,
                                  RedirectView,
                                  View)
from .models import Reply, Question
from .forms import QuestionForm, ReplyForm
from django.contrib.auth.mixins import LoginRequiredMixin
from videos.mixins import MemberRequiredMixin, StaffRequiredMixin


# Create your views here.
class QuestionCreateView(LoginRequiredMixin, CreateView):
    model = Question
    form_class = QuestionForm

    # def get_context_data(self, **kwargs):
    #     data = super(CourseCreateView, self).get_context_data(**kwargs)
    #     if self.request.POST:
    #         data['lecture'] = CourseLectureFormSet(self.request.POST)
    #     else:
    #         data['lecture'] = CourseLectureFormSet()
    #     return data

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return super(QuestionCreateView, self).form_valid(form)


class MYQuestionListView(LoginRequiredMixin, ListView):
    def get_queryset(self):
        user = self.request.user
        qs = Question.objects.filter(user=user).order_by('-id')
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(MYQuestionListView, self).get_context_data(*args, **kwargs)
        return context


class QuestionListView(LoginRequiredMixin, ListView):
    queryset = Question.objects.all()
    model = Question


class QuestionDetailView(LoginRequiredMixin, DetailView):
    model = Question
    queryset = Question.objects.all()


class QuestionUpdateView(LoginRequiredMixin, UpdateView):
    queryset = Question.objects.all()
    form_class = QuestionForm


class QuestionDeleteView(LoginRequiredMixin, DeleteView):
    queryset = Question.objects.all()
    success_url = reverse_lazy('questions:my_questions')


# Create your views here.
class ReplyCreateView(LoginRequiredMixin, CreateView):
    model = Reply
    form_class = ReplyForm

    # template_name = 'questions/questions_reply_form.html'

    def get_context_data(self, **kwargs):
        data = super(ReplyCreateView, self).get_context_data(**kwargs)
        data['question'] = Question.objects.get(pk=self.kwargs['question_id'])
        return data

    def form_valid(self, form):
        question = Question.objects.get(pk=self.kwargs['question_id'])
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.question = question
        obj.save()
        return super(ReplyCreateView, self).form_valid(form)


class ReplyUpdateView(LoginRequiredMixin, UpdateView):
    queryset = Reply.objects.all()
    form_class = ReplyForm

    def get_context_data(self, **kwargs):
        data = super(ReplyUpdateView, self).get_context_data(**kwargs)
        data['question'] = Reply.objects.get(pk=self.kwargs['pk']).question
        return data


class ReplyDeleteView(LoginRequiredMixin, DeleteView):
    queryset = Reply.objects.all()

    def get_success_url(self):
        return reverse_lazy('questions:detail', kwargs={'slug': self.object.question.slug})
