from django import template
from comments.models import Comment

register = template.Library()


@register.inclusion_tag('comments/snippets/load_comment.html')
def load_comment(url):
    qs = Comment.objects.filter(url=url)
    return {'comments': qs}
