from django.conf.urls import url
from .views import CommentTemplateView

urlpatterns = [
    url(r'^$', CommentTemplateView.as_view(), name='list'),
]
