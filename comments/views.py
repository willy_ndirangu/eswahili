from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.

class CommentTemplateView(TemplateView):
    template_name = 'comments/home.html'

