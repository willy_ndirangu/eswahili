$(document).ready(function () {
    var isUser = false
    var authUsername = ''
    var endpoint = $('.load_comments').attr('data-api-endpoint') || '/api/comments/';
    var dataUrl = $('.load_comments').attr('data-url');
    $('.load_comments').after("<div class='form_container'> </div>");
    getComment(dataUrl)

    function renderCommentLine(object) {
        var timestamp = new Date(object.timestamp).toLocaleString({hour: 'numeric', minute: 'numeric', hour12: true})
        author = ''
        if (object.user) {
            author = "<small>  by " + object.user.username + "</small>" + "<small> on " + timestamp + "</small>"
            if (object.user.username == authUsername) {
                author = author + ' <a href="#" class="comment-edit-button">Edit </a>'
            }
        }
        var html_ = "<div class='media'> <div class='media-body'>" + "<p class='comment-media-content' data-id='" + object.id + "'>" + object.content + "</p><br>" + author + "</div> </div> <br>"

        return html_

    }

    function getComment(requestUrl) {
        isUser = JSON.parse(getCookie('isUser'))
        authUsername = String(getCookie('authUsername'))

        $('.load_comments').html('<h1> Comments </h1>')
        $.ajax({
            method: "GET",
            url: endpoint,
            data: {
                url: requestUrl,
            },
            success: function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, object) {
                        $('.load_comments').append(renderCommentLine(object))
                    })


                }
                var formHtml = generateForm()
                $(".form_container").html(formHtml)

            },
            error: function (data) {
                console.log("error");
                console.log("data")
            }
        })

    }

    function generateForm() {
        if (isUser) {
            var html_ = "<form method='post' class='comment-form'>" + "<div class='form-group'>" +
                "<textarea class='form-control' placeholder='Your Comment ...' name='content'></textarea>" +
                // "<input type='hidden' name='user' value='{{request.user.id}}'>" +
                " <input type='submit'  class='btn btn-primary' value='Comment'>" + "</div>" + " </form>"

        }
        else {
            var html_ = '<div> <small>login to comment </small></div>'
        }


        return html_
    }

    function formatErrMsg(responseJSON) {
        var msg = responseJSON.substr(0, 50)


        var formattedMsg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
            msg +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            ' <span aria-hidden="true">&times;</span>' +
            '</button>' +
            '</div>'
        return formattedMsg
    }

    function handleForm(form_data) {
        // console.log(form_data)
        $.ajax({
            method: "POST",
            url: endpoint + "create/",
            data: form_data + '&url=' + dataUrl,
            success: function (data) {
                // console.log(data)
                // getComment(dataUrl)
                $('.load_comments').append(renderCommentLine(data))
                var formHtml = generateForm()
                $(".form_container").html(formHtml)

            },
            error: function (data) {
                console.log("error");
                // console.log(JSON.parse(data.responseText))
                var formErrorExists = $('.srvup-alert-error')
                if (formErrorExists.length > 0) {
                    formErrorExists.remove()
                }
                var msg = formatErrMsg(data.responseText)
                $('.comment-form textarea').before(msg)
            }
        })
    }

    function handleEditForm(form_data, objectId) {
        // console.log(form_data)
        $.ajax({
            method: "PUT",
            url: endpoint + objectId + "/",
            data: form_data,
            success: function (data) {
                // console.log(data)
                getComment(dataUrl)
                // $('.load_comments').append(renderCommentLine(data))
                // var formHtml = generateForm()
                // $(".form_container").html(formHtml)

            },
            error: function (data) {
                console.log("error");
                // console.log(JSON.parse(data.responseText))
                var formErrorExists = $('.srvup-alert-error')

                var msg = formatErrMsg(data.responseText)
                $('[data-id="' + objectId + '"] textarea').before(msg)
            }
        })
    }

    function generateEditForm(content, objectId) {
        var html_ = "<hr><form method='post' class='comment-edit-form' data-id='" + objectId + "'>" + "<div class='form-group'>" +
            "<textarea class='form-control' placeholder='Your Comment ...' name='content'>" + content + "</textarea>" +
            " <input type='submit' class='btn btn-primary' value='Save Edit'>" +
            "<button class='comment-cancel btn btn-warning '>Cancel </button>" +
            "<button class='comment-delete btn btn-danger '>Delete </button>" +
            "</div>" + " </form></<br>"
        return html_

    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    $(document).on('submit', '.comment-form', function (e) {
        e.preventDefault()
        var form_data = $(this).serialize()
        handleForm(form_data)

    })

    $(document).on('click', '.comment-edit-button', function (e) {
        e.preventDefault()
        $(this).fadeOut()
        var contentHolder = $(this).parent().parent().find('.comment-media-content')
        var contentTxt = contentHolder.text()
        var objectId = contentHolder.attr('data-id')
        $(this).after(generateEditForm(contentTxt, objectId))

    })

    $(document).on('submit', '.comment-edit-form', function (e) {
        e.preventDefault()
        var form_data = $(this).serialize()
        var objectId = $(this).attr('data-id')
        handleEditForm(form_data, objectId)

    })

    $(document).on('click', '.comment-delete', function (e) {
        e.preventDefault()
        var dataId = $(this).parent().parent().attr('data-id')
        console.log(dataId)
        $.ajax({
            method: 'DELETE',
            url: endpoint + dataId + '/',
            success: function () {
                getComment(dataUrl)

            }
        })

    })

    $(document).on('click', '.comment-cancel', function (e) {
        e.preventDefault()
        $(this).parent().parent().parent().find('.comment-edit-button').fadeIn()
        $(this).parent().remove()


    })


});

