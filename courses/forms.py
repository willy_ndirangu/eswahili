from django import forms
from django.forms import inlineformset_factory
from .models import Course, Lecture
from videos.models import Video


class LectureAdminForm(forms.ModelForm):
    class Meta:
        model = Lecture
        fields = ['free', 'order', 'title', 'video', 'description', 'slug']

    def __init__(self, *args, **kwargs):
        super(LectureAdminForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['free'].widget.attrs['class'] = 'form-control'
        self.fields['order'].widget.attrs['class'] = 'form-control'
        self.fields['video'].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget.attrs['class'] = 'form-control'
        self.fields['slug'].widget.attrs['class'] = 'form-control'
        obj = kwargs.get("instance")
        qs = Video.objects.all().unused()
        if obj:
            if obj.video:
                this_ = Video.objects.filter(pk=obj.video.pk)
                qs = (qs | this_)
            self.fields['video'].queryset = qs
        else:
            self.fields['video'].queryset = qs


class CourseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget.attrs['class'] = 'form-control'
        self.fields['category'].widget.attrs['class'] = 'form-control'
        self.fields['secondary'].widget.attrs['class'] = 'form-control'
        self.fields['image'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Course
        fields = ['title', 'description', 'category', 'secondary', 'image']
        exclude = ()

CourseFormSet= forms.modelformset_factory(Course,form=CourseForm,extra=1)
CourseLectureFormSet = inlineformset_factory(Course,Lecture, form=LectureAdminForm, formset=CourseFormSet, extra=1)
