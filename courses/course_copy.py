from courses.models import Course


def copy_course(qs=Course.objects.all()):
    if qs.count() < 10:
        for obj in qs:
            user = obj.user
            title = obj.title
            image = obj.image
            category = obj.category
            secondary = obj.secondary.all()
            description = obj.description
            price = obj.price
            new_obj = Course.objects.create(user=user,
                                            title=title,
                                            image=image,
                                            category=category,
                                            description=description,
                                            price=price)
            for cat in secondary:
                new_obj.secondary.add(cat)
            new_obj.save()
        qs2 = Course.objects.all()
        if qs2.count() < 10:
            return copy_course(qs2)
    return qs.count()
