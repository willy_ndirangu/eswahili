# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-08 06:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0010_auto_20190208_0604'),
    ]

    operations = [
        migrations.RenameField(
            model_name='course',
            old_name='categor',
            new_name='category',
        ),
    ]
