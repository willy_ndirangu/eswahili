# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-11 06:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0013_auto_20190208_0803'),
    ]

    operations = [
        migrations.AddField(
            model_name='lecture',
            name='free',
            field=models.BooleanField(default=False),
        ),
    ]
