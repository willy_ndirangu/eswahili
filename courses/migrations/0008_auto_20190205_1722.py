# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-05 17:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0007_mycourses'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mycourses',
            name='courses',
            field=models.ManyToManyField(blank=True, related_name='owned', to='courses.Course'),
        ),
    ]
