from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.http import Http404
from django.views.generic import (CreateView,
                                  DetailView,
                                  ListView,
                                  UpdateView,
                                  DeleteView,
                                  RedirectView,
                                  View)
from .models import Course, Lecture, MyCourses
from .forms import CourseForm, CourseLectureFormSet
from videos.mixins import MemberRequiredMixin, StaffRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from analytics.models import CourseViewEvent


# Create your views here.
class CourseCreateView(StaffRequiredMixin, CreateView):
    model = Course
    form_class = CourseForm

    # def get_context_data(self, **kwargs):
    #     data = super(CourseCreateView, self).get_context_data(**kwargs)
    #     if self.request.POST:
    #         data['lecture'] = CourseLectureFormSet(self.request.POST)
    #     else:
    #         data['lecture'] = CourseLectureFormSet()
    #     return data

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return super(CourseCreateView, self).form_valid(form)


class LectureDetailView(View):
    def get(self, request, cslug, lslug, *args, **kwargs):
        obj = None
        qs = Course.objects.filter(slug=cslug).lectures().owned(request.user)
        if not qs.exists():
            raise Http404
        course_ = qs.first()
        if request.user.is_authenticated():
            view_event, created = CourseViewEvent.objects.get_or_create(user=request.user, course=course_)
            if view_event:
                view_event.views += 1
                view_event.save()

        lectures_qs = course_.lecture_set.filter(slug=lslug)
        if not lectures_qs.exists():
            raise Http404
        obj = lectures_qs.first()
        context = {
            'object': obj,
            'course_': course_
        }
        if not course_.is_owner and not obj.free:
            return render(request, "courses/must_purchase.html", {"object": course_})
        return render(request, 'courses/lecture_detail.html', context)


class CourseDetailView(DetailView):
    queryset = Course.objects.all()

    def get_object(self):
        slug = self.kwargs.get('slug')
        qs = Course.objects.filter(slug=slug).lectures().owned(self.request.user)
        if qs.exists():
            obj = qs.first()
            if self.request.user.is_authenticated():
                view_event, created = CourseViewEvent.objects.get_or_create(user=self.request.user, course=obj)
                if view_event:
                    view_event.views += 1
                    view_event.save()
            return obj
        return Http404

    def get_context_data(self, *args, **kwargs):
        context = super(CourseDetailView, self).get_context_data(*args, **kwargs)
        return context


class CourseListView(ListView):
    paginate_by = 6

    def get_queryset(self):
        request = self.request
        qs = Course.objects.all()
        query = request.GET.get('q')
        user = self.request.user
        if query:
            qs = qs.filter(title__icontains=query).order_by('-id')
        if user.is_authenticated():
            qs = qs.order_by('-id')
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(CourseListView, self).get_context_data(*args, **kwargs)
        return context




class CourseUpdateView(StaffRequiredMixin, UpdateView):
    queryset = Course.objects.all()
    form_class = CourseForm


class CourseDeleteView(StaffRequiredMixin, DeleteView):
    queryset = Course.objects.all()
    success_url = reverse_lazy('courses:list')


class CoursePurchaseView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self, slug=None):
        qs = Course.objects.filter(slug=slug).owned(self.request.user)
        if qs.exists():
            user = self.request.user
            if user.is_authenticated():
                my_courses = user.mycourses
                # run transaction
                # if successful
                my_courses.courses.add(qs.first())
                return qs.first().get_absolute_url()
            return qs.first().get_absolute_url()
        return "/courses/"
