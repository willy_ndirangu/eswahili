from django.shortcuts import render
from django.views.generic import (CreateView,
                                  DetailView,
                                  ListView,
                                  UpdateView,
                                  DeleteView)
from .models import Video
from .forms import VideoForm
from .mixins import MemberRequiredMixin, StaffRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy


# Create your views here.
class VideoCreateView(StaffRequiredMixin, CreateView):
    model = Video
    form_class = VideoForm


class VideoDetailView(MemberRequiredMixin, DetailView):
    queryset = Video.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(VideoDetailView, self).get_context_data(*args, **kwargs)
        return context


class VideoListView(ListView):

    def get_queryset(self):
        request = self.request
        qs = Video.objects.all()
        query = request.GET.get('q')
        query_set = Q(title__icontains=query) | Q(title__contains=query)
        if query:
            qs = qs.filter(query_set)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(VideoListView, self).get_context_data(*args, **kwargs)
        return context


class VideoUpdateView(StaffRequiredMixin, UpdateView):
    queryset = Video.objects.all()
    form_class = VideoForm


class VideoDeleteView(StaffRequiredMixin, DeleteView):
    queryset = Video.objects.all()
    success_url = reverse_lazy('videos:list')
