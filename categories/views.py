from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (CreateView,
                                  DetailView,
                                  ListView,
                                  UpdateView,
                                  DeleteView,
                                  RedirectView)
from .models import Category
from .forms import CategoryForm
from videos.mixins import StaffRequiredMixin


# Create your views here.
# Create your views here.
class CategoryCreateView(StaffRequiredMixin, CreateView):
    model = Category
    form_class = CategoryForm


class CategoryListView(ListView):
    queryset = Category.objects.all()


class CategoryDetailView(DetailView):
    queryset = Category.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(*args, **kwargs)
        obj = context.get('object')
        qs1 = obj.primary_category.all().owned(self.request.user)
        qs2 = obj.secondary_category.all().owned(self.request.user)
        qs = (qs1 | qs2).distinct()
        context['featured_courses'] = qs1
        context['courses'] = qs
        return context


class CategoryUpdateView(StaffRequiredMixin, UpdateView):
    queryset = Category.objects.all()
    form_class = CategoryForm


class CategoryDeleteView(StaffRequiredMixin, DeleteView):
    queryset = Category.objects.all()
    success_url = reverse_lazy('categories:list')
