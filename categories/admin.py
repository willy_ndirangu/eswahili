from django.contrib import admin
from .models import Category
from .forms import CategoryAdminForm


# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    list_filter = ['updated', 'timestamp']
    list_display = ['title', 'updated', 'timestamp', 'order']
    readonly_fields = ['updated', 'timestamp', 'short_title']
    search_fields = ['title']
    list_editable = ['order']
    form = CategoryAdminForm

    def short_title(self, obj):
        return obj.title[:3]


admin.site.register(Category, CategoryAdmin)
