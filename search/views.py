from django.db.models import Q
from django.shortcuts import render
from django.views.generic import View

from categories.models import Category
from courses.models import Course, Lecture


# Create your views here.
class SearchView(View):
    def get(self, request, *args, **kwargs):
        qs = None
        c_qs = None
        query = request.GET.get('q')
        query_lookup = Q(title__icontains=query) | Q(category__title__icontains=query) | Q(
            description__icontains=query) | Q(lecture__title__icontains=query)
        if query:
            qs = Course.objects.all().lectures().filter(query_lookup).distinct()
            # c_qs = Category.objects.all().filter(title__icontains=query)
        return render(request, "search/default.html", {'qs': qs, 'c_qs': c_qs})
