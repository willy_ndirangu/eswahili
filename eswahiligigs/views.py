from django.views.generic.base import TemplateView
from django.views.generic import View
from django.shortcuts import render
from courses.models import Course
from analytics.models import CourseViewEvent
from allauth.account.views import PasswordChangeView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from analytics.models import CourseViewEvent
from courses.models import Course


class HomePageView(TemplateView):
    template_name = 'coming_soon.html'


class MainPageView(TemplateView):
    template_name = 'main.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['courses'] =Course.objects.all()[:6]
        return context


class DashboardPageView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/dashboard_base.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query = CourseViewEvent.objects.all()
        qs = query.filter(user=self.request.user).order_by('updated')[:3]
        if qs.exists():
            qs = qs
        else:
            qs = query.order_by('-views')[:3]
        context['courses_views'] = qs
        context['most_views']= query.order_by('-views')[:3]
        return context


class AccountPageView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/accounts_dashboard.html'


class HomeStagePageView(View):
    def get(self, request, *args, **kwargs):
        course_qs = Course.objects.all().lectures().owned(user=request.user)
        qs = course_qs.featured().distinct().lectures().order_by('?')[:6]
        event_qs = CourseViewEvent.objects.all().prefetch_related('course')
        if request.user.is_authenticated():
            event_views = event_qs.filter(user=request.user)
        else:
            event_views = event_qs.filter(views__gte=10)
        event_views = event_views.order_by('views')[:20]
        ids_ = [x.course.id for x in event_views]
        rec_courses = course_qs.filter(id__in=ids_).order_by('?')

        context = {
            'rec_courses': rec_courses,
            'qs': qs,
            'active': False
        }
        return render(request, 'home.html', context)


class LoginAfterPasswordChangeView(PasswordChangeView):
    @property
    def success_url(self):
        return reverse_lazy('account_login')


login_after_password_change = login_required(LoginAfterPasswordChangeView.as_view())
