from django import forms
from allauth.account.forms import (LoginForm, SignupForm, ChangePasswordForm, ResetPasswordForm as ResetFormPassword,
                                   SetPasswordForm, ResetPasswordKeyForm)


class CustomLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        self.fields['login'].widget.attrs['class'] = 'form-control'
        self.fields['login'].widget.attrs['placeholder'] = 'Your Email *'
        self.fields['password'].widget.attrs['placeholder'] = 'Your password *'
        self.fields['password'].widget.attrs['class'] = 'form-control'

    def login(self, *args, **kwargs):
        # Add your own processing here.

        # You must return the original result.
        return super(CustomLoginForm, self).login(*args, **kwargs)


class CustomSignupForm(SignupForm):
    def __init__(self, *args, **kwargs):
        super(CustomSignupForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['placeholder'] = 'Your Email *'
        self.fields['password1'].widget.attrs['placeholder'] = 'Your password *'
        self.fields['password2'].widget.attrs['placeholder'] = 'Repeat password *'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    def save(self, request):
        # Ensure you call the parent classes save.
        # .save() returns a User object.

        user = super(CustomSignupForm, self).save(request)

        # Add your own processing here.

        # You must return the original result.
        return user


class CustomChangePasswordForm(ChangePasswordForm):
    def __init__(self, *args, **kwargs):
        super(CustomChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['oldpassword'].widget.attrs['class'] = 'form-control'
        self.fields['oldpassword'].widget.attrs['placeholder'] = 'Your Old Password'
        self.fields['password1'].widget.attrs['placeholder'] = 'Your New Password *'
        self.fields['password2'].widget.attrs['placeholder'] = 'Repeat Password *'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    def save(self):
        # Ensure you call the parent classes save
        # .save() does not return anything
        super(CustomChangePasswordForm, self).save()

        # Add your own processing here.


class CustomResetPasswordForm(ResetFormPassword):
    def __init__(self, *args, **kwargs):
        super(CustomResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['placeholder'] = 'Your Email Address'


class CustomResetPasswordKeyForm(ResetPasswordKeyForm):
    def __init__(self, *args, **kwargs):
        super(CustomResetPasswordKeyForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'New Password'
        self.fields['password1'].widget.attrs['placeholder'] = 'Repeat Password'

        # Add your own processing here.
