"""eswahiligigs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from .views import HomePageView, HomeStagePageView, login_after_password_change, MainPageView, DashboardPageView, \
    AccountPageView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomePageView.as_view(), name='landing'),
    url(r'^main/$', MainPageView.as_view(), name='main'),
    url(r'^dashboard/account/user/$', AccountPageView.as_view(), name='account_dashboard'),
    url(r'^dashboard/$', DashboardPageView.as_view(), name='dashboard'),
    url(r'^accounts/password/change/$', login_after_password_change,
        name='account_change_password'),
    url('accounts/', include('allauth.urls')),
    url(r'^home/$', HomeStagePageView.as_view(), name='home'),
    url(r'^categories/', include('categories.urls', namespace='categories')),
    url(r'^courses/', include('courses.urls', namespace='courses')),
    url(r'^videos/', include('videos.urls', namespace='videos')),
    url(r'^search/', include('search.urls', namespace='search')),
    url(r'^payments/', include('billing.urls', namespace='payments')),
    url(r'^comments/', include('comments.urls', namespace='comments')),
    url(r'^api/comments/', include('comments.api.urls', namespace='api_comments')),
    url(r'^eswahiligigs/messages/', include('messages_eswahili.urls', namespace='eswahiligigs_messages')),
    url(r'^questions/', include('questions.urls', namespace='questions')),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
